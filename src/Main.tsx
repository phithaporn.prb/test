import { Suspense } from 'react'
import { BrowserRouter } from 'react-router-dom'

import MainExample from './routes/MainExample'

const Main = () => {
    return (
        <BrowserRouter basename={process.env.REACT_APP_APPLICATION_NAME}>
            <Suspense>
                <MainExample />
            </Suspense>
        </BrowserRouter>
    )
}

export default Main
