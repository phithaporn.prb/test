import ReactDOM from 'react-dom/client'
import './index.css'
import Main from './Main'
import reportWebVitals from './reportWebVitals'

//redux
import { Provider } from 'react-redux'
import ThemeProvider from '@mui/material/styles/ThemeProvider'
import store from './redux/store'
import theme from './utils/theme'

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)
root.render(
    <Provider store={store}>
        <ThemeProvider theme={theme}>
            <Main />
        </ThemeProvider>
    </Provider>,
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
