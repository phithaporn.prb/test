import { Box, Button, Checkbox, ListItem, Paper, styled, TextField, Typography } from '@mui/material'
import Grid from '@mui/material/Grid'
import { useEffect, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useStoreDispatch, useStoreSelector } from '../redux/useStore'
import { v4 as uuidv4 } from 'uuid'
import { deleteListStore, doneStatusStore, editListStore, setListStore } from '../redux/listReducer'

interface ListType {
    id: string
    job: string
    description: string
    date: string
    done: boolean
}

interface DetailType {
    list?: ListType
    mode: 'add' | 'edit'
}

const Assignment1Page = () => {
    const { control, handleSubmit, reset } = useForm<{ job: string; description: string }>({})

    const [detail, setDetail] = useState<DetailType>({ mode: 'add' })
    const [list, setList] = useState<ListType[]>([])
    const dispatch = useStoreDispatch()
    const listStore = useStoreSelector((state) => state.list)

    console.log('listStore', listStore)

    useEffect(() => {
        // if(listStore.list.length >0){
        setList(listStore.list)
        // }
    }, [listStore.list])

    const formatDate = (date?: Date) => {
        const dayjs = require('dayjs')
        require('dayjs/locale/th')
        dayjs.locale('th')

        return date && dayjs(date).format('DD MMM BBBB')
    }

    const onSave = (data: { job: string; description: string }) => {
        const { job, description } = data
        const format = {
            id: detail.list?.id ?? uuidv4(),
            job: job,
            description: description,
            date: formatDate(new Date()),
            done: detail.list?.done ?? false,
        }
        if (detail.mode === 'add') {
            dispatch(setListStore(format))
        } else {
            dispatch(editListStore(format))
        }
        setDetail({
            list: undefined,
            mode: 'add',
        })
        reset({ job: '', description: '' })
    }

    const onEdit = (itm: ListType) => {
        reset({
            job: itm.job,
            description: itm.description,
        })
        setDetail({
            list: itm,
            mode: 'edit',
        })
    }

    const onDelete = (id: string) => {
        dispatch(deleteListStore(id))
    }

    const onChecked = (id: string) => {
        dispatch(doneStatusStore(id))
    }

    const filterStatus = (action: 'done' | 'in' | 'all') => {
        if (action === 'all') {
            setList(listStore.list)
        } else {
            const filter = listStore.list.filter((itm) => {
                return action === 'done' ? itm.done : !itm.done
            })
            setList(filter)
        }
    }

    return (
        <div style={{ margin: '8px' }}>
            <form onSubmit={handleSubmit(onSave)}>
                <Grid container spacing={2}>
                    <Grid item xl={3} md={3}>
                        <Controller
                            render={({ field: { value, onChange } }) => {
                                return (
                                    <TextField
                                        id="job"
                                        type="text"
                                        size={'small'}
                                        label="job"
                                        placeholder={'job'}
                                        fullWidth
                                        value={value || ''}
                                        onChange={(values: any) => {
                                            onChange(values.target.value)
                                        }}
                                    />
                                )
                            }}
                            name={'job'}
                            control={control}
                        />
                    </Grid>
                    <Grid item xl={3} md={3}>
                        <Controller
                            render={({ field: { value, onChange } }) => {
                                return (
                                    <TextField
                                        id="description"
                                        type="text"
                                        size={'small'}
                                        label="description"
                                        placeholder={'description'}
                                        fullWidth
                                        value={value || ''}
                                        onChange={(values: any) => {
                                            onChange(values.target.value)
                                        }}
                                    />
                                )
                            }}
                            name={'description'}
                            control={control}
                        />
                    </Grid>
                    <Grid item xl={1}>
                        <Button variant="outlined" type="submit">
                            Send
                        </Button>
                    </Grid>
                    <Grid item xl={1}>
                        <Button variant="outlined" onClick={() => filterStatus('in')}>
                            Inprogress
                        </Button>
                    </Grid>
                    <Grid item xl={1}>
                        <Button variant="outlined" onClick={() => filterStatus('done')}>
                            Done
                        </Button>
                    </Grid>
                    <Grid item xl={1}>
                        <Button variant="outlined" onClick={() => filterStatus('all')}>
                            All
                        </Button>
                    </Grid>
                </Grid>
            </form>

            <Grid container spacing={2}>
                {list.length > 0 &&
                    list.map((itm) => {
                        return (
                            <ListItemStyled key={uuidv4()}>
                                <Paper
                                    variant="outlined"
                                    sx={{
                                        width: '50%',
                                        borderLeft: 5,
                                    }}
                                >
                                    <Box sx={{ ml: '4px', mr: '4px' }}>
                                        <Grid container sx={{ p: '8px', pb: '8px', pt: '4px' }}>
                                            <Checkbox checked={itm.done} onClick={() => onChecked(itm.id)} />
                                            <p>Job: {itm.job}</p>
                                        </Grid>
                                        <Grid container sx={{ p: '8px', pb: '8px', pt: '4px' }}>
                                            <Grid container spacing={0} paddingBottom={'8px'}>
                                                <Grid item xs={6}>
                                                    <Typography>Description: {itm.description}</Typography>
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <Typography>Date: {itm.date}</Typography>
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <Typography style={{ color: itm.done ? 'green' : '#DABE08' }}>
                                                        Status: {itm.done ? 'done' : 'inprogress'}
                                                    </Typography>
                                                </Grid>
                                                <Grid item xs={3}>
                                                    <Button
                                                        onClick={() => {
                                                            onEdit(itm)
                                                        }}
                                                    >
                                                        แก้ไข
                                                    </Button>
                                                </Grid>
                                                <Grid item xs={3}>
                                                    <Button
                                                        onClick={() => {
                                                            onDelete(itm.id)
                                                        }}
                                                    >
                                                        ลบ
                                                    </Button>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Box>
                                </Paper>
                            </ListItemStyled>
                        )
                    })}
            </Grid>
        </div>
    )
}

export default Assignment1Page

const ListItemStyled = styled(ListItem)(() => ({
    borderRadius: '5px',
    '& .MuiTouchRipple-root': {
        borderRadius: '5px',
    },
    marginTop: '16px',
}))
