import { Route, Routes } from 'react-router-dom'
import Assignment1Page from '../pages/Assignment1'

const MainExample = () => {
    return (
        <Routes>
            <Route path="/">
                <Route index element={<Assignment1Page />} />
            </Route>
        </Routes>
    )
}

export default MainExample
