import { createSlice, ActionCreator } from '@reduxjs/toolkit'

interface ListTypeState {
    id: string
    job: string
    description: string
    date: string
    done: boolean
}

type ListState = {
    list: ListTypeState[]
}

const initialState: ListState = {
    list: [],
}

const listSlice = createSlice({
    name: 'list',
    initialState,
    reducers: {
        setListStore(state, actions) {
            return {
                list: [...state.list, actions.payload],
            }
        },
        editListStore(state, actions) {
            return {
                list: state.list.map((item) => {
                    if (item.id === actions.payload.id) {
                        return {
                            ...item,
                            ...actions.payload,
                        }
                    } else {
                        return item
                    }
                }),
            }
        },
        deleteListStore(state, actions) {
            return {
                list: state.list.filter((item) => item.id !== actions.payload),
            }
        },
        doneStatusStore(state, actions) {
            return {
                list: state.list.map((item) => {
                    if (item.id === actions.payload) {
                        return {
                            ...item,
                            done: !item.done,
                        }
                    } else {
                        return item
                    }
                }),
            }
        },
        resetListStore(state) {
            return initialState
        },
    },
})

export const { setListStore, resetListStore, editListStore, deleteListStore, doneStatusStore } = listSlice.actions
export default listSlice.reducer
