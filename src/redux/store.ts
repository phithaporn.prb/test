import { configureStore } from '@reduxjs/toolkit'
import listReducer from './listReducer'

export const store = configureStore({
    reducer: {
        list: listReducer,
    },
})

export type StoreDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>
export default store
